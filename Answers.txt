1. Most likely one of the submitted transaction will recieve an error. In event that both made it to queue of transaction pool of both nodes, either one will be eventually dropped once one of them finally get to transaction pool where the unfortunate transaction gets a nonce error.

2. If one of them made it to the network transaction pool and the later has higher gas price, given the one that got queued is still on pending status - it will be overriden.

3. Makes a transaction unique and prevents the user from sending same multiple transaction by mistake and thus spending unnecessary amount of gas and ether

4. As far as I know, block info should immutable once mined.