const Migrations = artifacts.require("Migrations");

module.exports = async function (deployer, network) {
  console.log(`1: Deploying to ${network}`);
  await deployer.deploy(Migrations);
};
