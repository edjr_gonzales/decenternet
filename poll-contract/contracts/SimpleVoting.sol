// SPDX-License-Identifier: GPL-3.0

pragma solidity >=0.7.0 <0.9.0;
pragma abicoder v2;

contract SimpleVoting {
    event UserVoted(
        address indexed voter, 
        uint256 indexed poll,
        uint256 indexed timestamp,
        string choice
    );

    event ChangeVote(
        address indexed voter,
        uint256 indexed poll,
        uint256 indexed timestamp,
        string newChoice,
        string oldChoice
    );

    struct VoteEntry {
        address voter;
        uint choice;
        uint256 castedOn;
        uint256 voteIndex;
    }

    struct PollOption {
        string name;
        bool exists;

        uint256 voteCount;
        address[] voterList;
    }

    struct Poll {
        string name;
        uint256 deadline;
        
        uint256 totalVotes;

        string[] choices;
        mapping( string => PollOption ) pollChoices;

        mapping( address => VoteEntry ) votes;
        
        // mapping( uint => uint256 ) breakdown;

        // choice -> array of voters
        // mapping( uint => address[] ) choiceVoterList;

    }

    mapping( uint256 => Poll ) public polls;

    uint256 internal totalPolls;

    constructor() {
        totalPolls = 0;
    }

    function totalPoll() external view returns (uint256) {
        return totalPolls;
    }

    function isPollEnded(uint256 pollId) external view returns (bool) {
        return polls[pollId].deadline < block.timestamp;
    }

    function pollDeadline(uint256 pollId) external view returns (uint256) {
        return polls[pollId].deadline;
    }

    function pollTime() external view returns (uint256) {
        return block.timestamp;
    }

    function getPollChoices(uint256 pollId) public view returns (string[] memory) {
        return polls[pollId].choices;
    }

    function getPollName(uint256 pollId) public view returns (string memory) {
        return polls[pollId].name;
    }

    function getPollEntryVoters(uint256 pollId, uint choice) external view returns (address[] memory) {
        require(polls[pollId].choices.length > choice, "POLL_CHOICE_INDEX_INVALID");

        string memory choiceName = polls[pollId].choices[choice];

        return polls[pollId].pollChoices[choiceName].voterList;
    }

    function getPollEntryVoterCount(uint256 pollId, uint choice) external view returns (uint256) {
        require(polls[pollId].choices.length > choice, "POLL_CHOICE_INDEX_INVALID");

        string memory choiceName = polls[pollId].choices[choice];

        return polls[pollId].pollChoices[choiceName].voteCount;
    }

    function createPoll
    (
        string memory name,
        string[] memory choices,
        uint32 deadline
    ) external returns (uint256 pollId) {
        pollId = totalPolls++;
        
        Poll storage p = polls[pollId]; 

        p.name = name;
        p.deadline = deadline;
        p.choices = choices;

        for (uint256 j = 0; j < choices.length; j++) {
            address[] memory voters;

            p.pollChoices[choices[j]] = PollOption
            (
                choices[j],
                true,
                0,
                voters
            );
        }
    }

    function castVote(uint256 pollId, uint choice) external {
        require(polls[pollId].deadline > block.timestamp, "POLL_IS_FINISHED");
        require(polls[pollId].choices.length > choice, "INVALID_VOTE");
        
        VoteEntry storage entry = polls[pollId].votes[msg.sender];

        bool voteExists = entry.castedOn > 0;
        uint oldChoice = entry.choice;

        string memory oldChoiceName = polls[pollId].choices[oldChoice];
        string memory newChoiceName = polls[pollId].choices[choice];

        if(voteExists) {
            require(oldChoice != choice, "CANT_VOTE_TWICE");

            // already voted, changing vote
            polls[pollId].pollChoices[oldChoiceName].voteCount--; // deduct previous vote from former vote
            polls[pollId].pollChoices[oldChoiceName].voterList[entry.voteIndex] = address(0); // unset address from voter list
        } else {
            entry.voter = msg.sender;
            polls[pollId].totalVotes++;
        }

        entry.choice = choice;
        entry.castedOn = block.timestamp;
        entry.voteIndex = polls[pollId].pollChoices[newChoiceName].voterList.length;
        
        polls[pollId].pollChoices[newChoiceName].voteCount++;
        polls[pollId].pollChoices[newChoiceName].voterList.push(msg.sender);

        if(voteExists){
            emit ChangeVote(msg.sender, pollId, entry.castedOn, newChoiceName, oldChoiceName);
        } else {
            emit UserVoted(msg.sender, pollId, entry.castedOn, newChoiceName);
        }
    }

}