## Compiling

First, install npm packages:

```bash
npm install
# or
yarn install
```

Then, compile:

```bash
npx truffle compile
```

OR, deploy to rinkeby:

```bash
npx truffle migrate --network rinkeby
```
