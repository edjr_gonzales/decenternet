import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import Web3 from 'web3';
import { Contract } from "web3-eth-contract";

import { RootState } from '../store';

interface Web3State {
  loading: boolean;
  contract?: Contract;
  web3?: Web3;
  account?: string;
}

const initialState: Web3State = {
  loading: true,
}

export const web3Slice = createSlice({
  name: 'web3',
  initialState,
  reducers: {
    setWeb3: (state, action: PayloadAction<{ web3: Web3, account: string }>) => {
      state.web3 = action.payload.web3;
      state.account = action.payload.account;
      if(state.contract && state.web3){
        state.loading = false;
      }
    },
    setContract: (state, action: PayloadAction<Contract>) => {
      state.contract = action.payload;
      if(state.contract && state.web3){
        state.loading = false;
      }
    },
  },
});

export const { setWeb3, setContract } = web3Slice.actions;
export const votingContract = (state: RootState) => state.web3.contract;
export const web3 = (state: RootState) => state.web3.web3;
export default web3Slice.reducer;
