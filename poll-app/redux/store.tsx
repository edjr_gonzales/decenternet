import { configureStore } from '@reduxjs/toolkit';
import web3Reducer from "./features/web3";

const store = configureStore({
  reducer: {
    web3: web3Reducer,
  },
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware({
      serializableCheck: {
        // Ignore these action types
        // ignoredActions: ['your/action/type'],
        ignoredActions: [],
        // Ignore these field paths in all actions
        ignoredActionPaths: ['payload.web3', 'payload.contract', 'payload'],
        // Ignore these paths in the state
        // ignoredPaths: ['items.dates'],
        ignoredPaths: ['we3State.web3', 'we3State.contract', 'web3.web3', 'web3.contract'],
      },
    }),
});


export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;

export default store;