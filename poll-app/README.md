## Running Web App

First, isntall npm packages:
```bash
npm install
# or
yarn install
```

Then, run the development server:

```bash
npm run dev
# or
yarn dev
```
Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.


## Vote Monitoring Script

Run script with:
```bash
npm run monitor
# or
yarn monitor
```