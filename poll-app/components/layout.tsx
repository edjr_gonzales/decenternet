import { useAppDispatch, useAppSelector } from '../redux/hooks';
import { Alert, BottomNavigation, BottomNavigationAction, Box, Container, Grid, Skeleton } from '@mui/material';
import LeaderboardIcon from '@mui/icons-material/Leaderboard';
import { useEffect, useState } from 'react';
import Web3 from 'web3';
import { setWeb3, setContract } from '../redux/features/web3';

import SimpleVoting from "../../poll-contract/build/contracts/SimpleVoting.json";

declare global {
  interface Window { 
    ethereum?: any; 
    web3?: any;
  }
}

const Layout = ({ children }: any) => {
  const dispatch = useAppDispatch();

  const web3Loading = useAppSelector((state) => state.web3.loading);
  const votingContract = useAppSelector((state) => state.web3.contract);
  const web3 = useAppSelector((state) => state.web3.web3);
  const account = useAppSelector((state) => state.web3.account);

  const [ web3Error, setWeb3Error ] = useState<false | string>(false);
  const [value, setValue] = useState<number>(0);

  const enableEthereum = async (ethereum:any) => {
    await ethereum.request({method: 'eth_requestAccounts'});
    const web3 = window.web3 = new Web3(ethereum);

    web3.eth.getAccounts((error, accounts) => {
      if(error){
        setWeb3Error(error.message);
      } else {
        dispatch(
          setWeb3({ web3: web3, account: accounts[0] })
        );
      }
    });
  }

  const enableWeb3 = async (windowWeb3:any) => {
    const web3 = window.web3 = new Web3(windowWeb3.currentProvider);

    window.ethereum.enable();
    
    web3.eth.getAccounts((error, accounts) => {
      if(error){
        setWeb3Error(error.message);
      } else {
        dispatch(
          setWeb3({ web3: web3, account: accounts[0] })
        );
      }
    });
  }

  useEffect(() => {
    if(!votingContract){
      if (typeof window.ethereum === 'undefined') {
        setWeb3Error('Metamask is required for this site to run.');

      } else if(window.ethereum) {
        enableEthereum(window.ethereum);
        
      } else if(window.web3) {
        enableWeb3(window.web3);
      }
    }
  }, []);

  useEffect(() => {
    if(web3){ // web3 has been loaded, now load the contract
      web3.eth.net.getId((error, id) => {
        if(error){
          setWeb3Error(error.message);
        } else {
          const netKey = `${id}`;
          if(netKey in SimpleVoting.networks){
            const address = SimpleVoting.networks[netKey].address;
            const contract = new web3.eth.Contract(SimpleVoting.abi, address);

            dispatch(setContract(contract));
          } else {
            setWeb3Error(`Voting contract is not deployed for this blockhain network ${id}. Please switch to supported network.`);
          }
        }
      })
      
    }
  }, [web3]);


  if(web3Error){
    return (
      <Container>
        <Alert severity="error">{web3Error}</Alert>
      </Container>
    );
  }

  if(!web3Loading){
    return (
      <Container maxWidth="lg">
        <Grid container spacing={2} style={{
          minHeight: '80vh'
        }}>
          <Grid item xs={12}>
            <Alert severity="info">Wallet: {account}</Alert>
            {children}
          </Grid>
        </Grid>
        <BottomNavigation
          showLabels
          value={value}
          onChange={(event, newValue) => {
            setValue(newValue);
          }}
        >
          <BottomNavigationAction label="Polls" icon={<LeaderboardIcon />} />
        </BottomNavigation>
      </Container>
    )
  }

  return (
    <Box
      sx={{
        width: 300,
        height: 300,
        backgroundColor: 'primary.dark',
        '&:hover': {
          backgroundColor: 'primary.main',
          opacity: [0.9, 0.8, 0.7],
        },
      }}
    >
      <Skeleton variant="rectangular" width={300} height={300} />
    </Box>
  );
}

export default Layout;