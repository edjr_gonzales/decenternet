require('dotenv').config();

const Web3 = require('web3');
const HDWalletProvider = require('@truffle/hdwallet-provider');
const SimpleVoting = require("../../poll-contract/build/contracts/SimpleVoting.json");

const INFURA_KEY = process.env.INFURA_KEY;
const MNEMONIC = process.env.MNEMONIC;

const wsProvider = new Web3.providers.WebsocketProvider(`wss://rinkeby.infura.io/ws/v3/2dd2f51302b34f359fc1d37af63a3427`);

const web3 = new Web3(wsProvider);

const buildContract = async () => {
  const netId = await web3.eth.net.getId();
  return new web3.eth.Contract(SimpleVoting.abi, SimpleVoting.networks[netId].address);
}

const main = async () => {
  const contract = await buildContract();
  contract.events.UserVoted({}, (err, arg) => {
    /* console.log('error', err);
    console.log('arg', arg); */
  })
    .on('data', (event) => {
      const { voter, poll, choice } = event.returnValues;
      console.log('Vote Changed');
      console.log('\tPoll ID', poll);
      console.log('\tVoter', voter);
      console.log(`\tVoted`, choice);
    });

  contract.events.ChangeVote({}, (err, arg) => {
    /* console.log('error', err);
    console.log('arg', arg); */
  })
    .on('data', (event) => {
      const { voter, poll, oldChoice, newChoice } = event.returnValues;
      console.log('Vote Changed');
      console.log('\tPoll ID', poll);
      console.log('\tVoter', voter);
      console.log(`\tChanges Vote from '${newChoice}' to '${oldChoice}'`); // bug deployed
    });
}

main();