import '../styles/globals.css'

import type { AppProps } from 'next/app';
import store from '../redux/store';
import { Provider } from 'react-redux'
import Layout from '../components/layout';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import { CacheProvider } from '@emotion/react';
import CssBaseline from '@mui/material/CssBaseline';
import { red } from '@mui/material/colors';
import createCache from '@emotion/cache';

const MyApp = ({ Component, pageProps }: AppProps) => {
  const theme = createTheme({
    palette: {
      primary: {
        main: '#556cd6',
      },
      secondary: {
        main: '#19857b',
      },
      error: {
        main: red.A400,
      },
    },
  });
  
  const emotionCache = createCache({ key: 'css', prepend: true });
  
  return (
    <Provider store={store}>
      <CacheProvider value={emotionCache}>
        <ThemeProvider theme={theme}>
          <Layout>
            <CssBaseline />
            <Component {...pageProps} />
          </Layout>
        </ThemeProvider>
      </CacheProvider>
    </Provider>
  );
}

export default MyApp;
