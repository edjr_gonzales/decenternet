import { AccountCircle } from "@mui/icons-material";
import { Alert, Button, Grid, List, ListItem, ListItemIcon, ListItemText, Paper, Typography } from "@mui/material";
import { GetServerSideProps, InferGetServerSidePropsType } from "next";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import { useAppSelector } from "../../redux/hooks";

interface PollEntry {
  id: number;
  name: string;
  choices: string[];
  deadline: number;
  totalVotes:number;
  choiceBreakdown: {
    [choice: string]: string[]
  }
}

const ZEROADDR = '0x0000000000000000000000000000000000000000';

const VoteStats = ({ poll }: InferGetServerSidePropsType<typeof getServerSideProps>) => {
  const router = useRouter();
  
  const votingContract = useAppSelector((state) => state.web3.contract);
  
  const [ pollChoiceIndex, setPollChoiceIndex ] = useState<boolean>(false);
  const [ errorMsg, setErrorMsg ] = useState<string>('');
  const [pollEntry, setPollEntry] = useState<PollEntry>({
    id: Number(poll),
    name: '',
    choices: [],
    deadline: 0,
    totalVotes: 0,
    choiceBreakdown: {},
  });

  const getPollChoiceVoters = async () => {
    const result: any = {};

    for(let i = 0; i < pollEntry.choices.length; i++){
      const choiceName = pollEntry.choices[i];
      const voters: string[] = await votingContract?.methods.getPollEntryVoters(pollEntry.id, i).call();

      result[choiceName] = voters.filter(voter => voter !== ZEROADDR);
    }

    setPollEntry({
      ...pollEntry,
      choiceBreakdown: result,
    });
  }

  useEffect(() => {
    votingContract?.methods.polls(pollEntry.id).call()
        .then(({name, deadline, totalVotes}) => {
          
          pollEntry.name = name;
          pollEntry.deadline = Number(deadline);
          pollEntry.totalVotes = Number(totalVotes);
          
          return votingContract?.methods.getPollChoices(pollEntry.id).call();
        })
        .then((choices:string[]) => {
          pollEntry.choices = choices;

          setPollEntry({
            ...pollEntry
          });
        })
        .catch((error:any) => {
          setErrorMsg('Get Poll Votes: ' +  + ' ' + error.toString());
        });
  }, []);

  useEffect(() => {
    if(pollEntry.choices.length > 0){
      getPollChoiceVoters();
    }
  }, [pollEntry.choices]);

  if(errorMsg !== ''){
    return <Alert severity="error">{errorMsg}</Alert>;
  }

  if(pollEntry.deadline == 0){
    return <Alert severity="info">Loading...</Alert>;
  }

  return (
    <Grid container>
      <Grid item xs={12} padding={2}>
        <Button variant="contained"  color="warning" onClick={(e) => {
          router.push('/');
        }}>Return To List</Button>
      </Grid>

      <Grid item xs={12} padding={2}>
        <Paper>
          <Typography variant="h3" component="h4">{pollEntry.name}</Typography>  
        
          <Grid container xs={12} padding={2}>
          {
            pollEntry.choices.map((entry, index) => {
              return (
                <Grid container xs={12} padding={2}>
                  <Paper>
                    <Typography variant="h4">{entry} ({pollEntry.choiceBreakdown[entry]?.length || 0} Votes)</Typography>
                    <List dense={true}>
                      {
                        (pollEntry.choiceBreakdown[entry] || []).map(wallet => (
                          <ListItem>
                            <ListItemIcon>
                              <AccountCircle />
                            </ListItemIcon>
                            <ListItemText
                              primary={wallet}
                              secondary={`Wallet ${wallet}`}
                            />
                          </ListItem>
                        ))
                      }
                    </List>
                  </Paper>
                </Grid>
              );
            })
          }
          </Grid>
        </Paper>
      </Grid>

      
    </Grid>
  );

}

export const getServerSideProps: GetServerSideProps = async (context) => {
  const { id } = context.query;
  
  return {
    props: {
      poll: id,
    },
  };
};

export default VoteStats;