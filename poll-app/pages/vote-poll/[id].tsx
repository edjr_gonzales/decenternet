import { Alert, Button, Grid, Paper, Typography } from "@mui/material";
import { GetServerSideProps, InferGetServerSidePropsType } from "next";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import { useAppSelector } from "../../redux/hooks";

interface PollEntry {
  id: number;
  name: string;
  choices: string[];
  deadline: number;
  totalVotes:number;
}

const VoteForm = ({ poll }: InferGetServerSidePropsType<typeof getServerSideProps>) => {
  const router = useRouter();
  
  const votingContract = useAppSelector((state) => state.web3.contract);
  const account = useAppSelector((state) => state.web3.account);
  
  const [ voting, setVoting ] = useState<boolean>(false);
  const [ errorMsg, setErrorMsg ] = useState<string>('');
  const [pollEntry, setPollEntry] = useState<PollEntry>({
    id: Number(poll),
    name: '',
    choices: [],
    deadline: 0,
    totalVotes: 0,
  });

  useEffect(() => {
    votingContract?.methods.polls(pollEntry.id).call()
        .then(({name, deadline, totalVotes}) => {
          
          pollEntry.name = name;
          pollEntry.deadline = Number(deadline);
          pollEntry.totalVotes = Number(totalVotes);
          
          return votingContract?.methods.getPollChoices(pollEntry.id).call();
        })
        .then((choices:string[]) => {
          pollEntry.choices = choices;

          setPollEntry({
            ...pollEntry
          });
        })
        .catch((error:any) => {
          setErrorMsg('Get Poll: ' +  + ' ' + error.toString());
        });
  }, []);

  const vote = (choiceIndex:number) => {
    setVoting(true);
    votingContract?.methods.castVote(pollEntry.id, choiceIndex).send({ from: account })
      .then((receipt) => {
        router.push('/');
      })
      .catch((error:any) => {
        setErrorMsg(error.toString());
      })
  }

  if(errorMsg !== ''){
    return <Alert severity="error">{errorMsg}</Alert>;
  }

  if(pollEntry.deadline == 0){
    return <Alert severity="info">Loading...</Alert>;
  }

  if(voting){
    return <Alert severity="info">Casting Vote...</Alert>;
  }

  return (
    <Grid container>
      <Grid item xs={12} padding={2}>
        <Button variant="contained"  color="warning" onClick={(e) => {
          router.push('/');
        }}>Return To List</Button>
      </Grid>

      <Grid item xs={12} padding={2}>
        <Paper>
          <Typography variant="h3" component="h4">{pollEntry.name}</Typography>  
        
          <Grid container xs={12} padding={2}>
          {
            pollEntry.choices.map((entry, index) => {
              return (
                <Grid container xs={12} padding={2}>
                  <Button variant="contained" onClick={(e) => {
                    vote(index);
                  }}>Vote for {entry}</Button>
                </Grid>
              );
            })
          }
          </Grid>
        </Paper>
      </Grid>

      
    </Grid>
  );

}

export const getServerSideProps: GetServerSideProps = async (context) => {
  const { id } = context.query;
  
  return {
    props: {
      poll: id,
    },
  };
};

export default VoteForm;