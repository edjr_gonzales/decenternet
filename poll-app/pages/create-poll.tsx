import { Alert, Button, Grid, Typography } from "@mui/material";
import { useEffect, useState } from "react";
import { useAppSelector } from "../redux/hooks";
import { DateTime } from 'luxon';
import { useRouter } from 'next/router';

interface PollEntry {
  id: number;
  name: string;
  choices: string[];
  deadline: number;
  totalVotes:number;
}

const CreatePoll = (props:any) => {
  const router = useRouter();

  const votingContract = useAppSelector((state) => state.web3.contract);

  const [ polls, setPolls ] = useState<PollEntry[]>([]);
  const [ totalPolls, setTotalPolls ] = useState<number>(0);
  const [ currentPollId, setCurrentPollId ] = useState<number>(0);
  const [ errorMsg, setErrorMsg ] = useState<string>('');
  
  useEffect(() => {
    votingContract?.methods.totalPoll().call()
      .then((total:number) => {
        setTotalPolls(total);
      })
      .catch((error:any) => {
        setErrorMsg('Total Poll: ' + error.toString());
      });
  }, []);

  useEffect(() => {
    if(totalPolls > 0){
      const poll:PollEntry = {
        id: currentPollId,
        name: '',
        choices: [],
        deadline: 0,
        totalVotes: 0,
      }

      votingContract?.methods.poll(currentPollId).call()
        .then((name:string, deadline:number, totalVotes:number) => {
          poll.name = name;
          poll.deadline = deadline;
          poll.totalVotes = totalVotes;
          
          return votingContract?.methods.getPollChoices(currentPollId).call();
        })
        .then((choices:string[]) => {
          console.log('choices', choices);

          poll.choices = choices;

          setPolls([...polls, poll]);
          if(currentPollId + 1 < totalPolls){
            setCurrentPollId(currentPollId + 1);
          }
        })
        .catch((error:any) => {
          setErrorMsg('Get Poll: ' +  + ' ' + error.toString());
        });
    }
  }, [totalPolls, currentPollId]);

  if(errorMsg !== ''){
    return <Alert severity="error">{errorMsg}</Alert>;
  }
  
  return (
    <Grid container>
      <Grid item xs={12}>
        <Button variant="contained"  color="warning" onClick={(e) => {
          router.push('/');
        }}>Cancel</Button>
      </Grid>
      <Grid item xs={12}>
        
      </Grid>
    </Grid>
    
  );

}

export default CreatePoll;