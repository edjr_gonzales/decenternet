import { Alert, Button, Grid, Typography } from "@mui/material";
import { useEffect, useState } from "react";
import { useAppSelector } from "../redux/hooks";
import { DateTime } from 'luxon';
import { useRouter } from 'next/router';

import MUIDataTable, {
  MUIDataTableColumnDef,
  MUIDataTableOptions,
} from "mui-datatables";

interface PollEntry {
  id: number;
  name: string;
  choices: string[];
  count: number[];
  deadline: number;
  totalVotes:number;
}

const options: MUIDataTableOptions = {
  sortOrder: {
    name: "ID",
    direction: "asc",
  },
  rowsPerPage: 100,
  rowsPerPageOptions: [25, 50, 100],
  fixedHeader: true,
  fixedSelectColumn: true,
};

const Index = (props:any) => {
  const router = useRouter();

  const votingContract = useAppSelector((state) => state.web3.contract);

  const [ polls, setPolls ] = useState<PollEntry[]>([]);
  const [ totalPolls, setTotalPolls ] = useState<number>(0);
  const [ currentPollId, setCurrentPollId ] = useState<number>(0);
  const [ errorMsg, setErrorMsg ] = useState<string>('');
  
  useEffect(() => {
    votingContract?.methods.totalPoll().call()
      .then((total:number) => {
        setTotalPolls(total);
      })
      .catch((error:any) => {
        setErrorMsg('Total Poll: ' + error.toString());
      });
  }, []);

  useEffect(() => {
    if(totalPolls > 0 && currentPollId < totalPolls){
      const idFound = polls.find(p => p.id == currentPollId);

      if(idFound){
        return;
      }
      
      const poll:PollEntry = {
        id: currentPollId,
        name: '',
        choices: [],
        count: [],
        deadline: 0,
        totalVotes: 0,
      }

      votingContract?.methods.polls(currentPollId).call()
        .then(({name, deadline, totalVotes}) => {
          poll.name = name;
          poll.deadline = Number(deadline);
          poll.totalVotes = Number(totalVotes);
          
          return votingContract?.methods.getPollChoices(currentPollId).call();
        })
        .then(async (choices:string[]) => {
          poll.choices = choices;

          for(let i = 0; i < choices.length; i++){
            const count = await votingContract?.methods.getPollEntryVoterCount(currentPollId, i).call();
            poll.count.push(count);
          }
          
          setPolls([...polls, poll]);
          if(currentPollId + 1 < totalPolls){
            setCurrentPollId(currentPollId + 1);
          }
        })
        .catch((error:any) => {
          setErrorMsg('Get Poll: ' +  + ' ' + error.toString());
        });
    }
  }, [totalPolls, currentPollId]);

  if(errorMsg !== ''){
    return <Alert severity="error">{errorMsg}</Alert>;
  }

  const columns: MUIDataTableColumnDef[] = [
    {
      name: "id",
      label: "ID"
    },
    {
      name: "name",
      label: "Poll Name",
      options: {
        viewColumns: true,
        filter: true,
      },
    },
    {
      name: "choices",
      label: "Choices",
      options: {
        customBodyRender: (value: string[], meta) => {
          return (
            <ul style={{ minWidth: '100px' }}>
              { value.map((v, i) => <li style={{ paddingBottom: "10px" }}>
                <strong>{v}</strong>
                <br />
                {polls[meta.rowIndex].count[i]} Votes
              </li>) }
            </ul>
          );
        },
      },
    },
    {
      name: "totalVotes",
      label: "Votes",
    },
    {
      name: "deadline",
      label: "Ends On",
      options: {
        customBodyRender: (value) => {
          return DateTime.fromSeconds(Number(value)).toRFC2822();
        },
      },
    },
    {
      name: "deadline",
      label: "Ended",
      options: {
        customBodyRender: (value: number) => {
          return Number(value) > DateTime.now().toSeconds() ? 'On-Going' : 'YES' ;
        },
      },
    },
    {
      name: "deadline",
      label: "Vote",
      options: {
        customBodyRender: (value:number, meta) => {
          return Number(value) > DateTime.now().toSeconds() ? <Button variant="contained" onClick={(e) => {
            router.push(`/vote-poll/${meta.rowData[0]}`);
          }}>Vote</Button> : null;
        },
      },
    },
    {
      name: "id",
      label: "Stats",
      options: {
        customBodyRender: (value:number) => {
          return <Button variant="contained" onClick={(e) => {
            router.push(`/vote-stats/${value}`);
          }}>Stats</Button>;
        },
      },
    },
  ];
  
  return (
    <Grid container>
      <Grid item xs={12} padding={2}>

        {/* <Button variant="contained" onClick={(e) => {
          router.push('/create-poll');
        }}>Create Poll</Button> */}

      </Grid>
      <Grid item xs={12}>
        <MUIDataTable
          title={<Typography variant="h6">Poll Listing</Typography>}
          data={polls}
          columns={columns}
          options={options}
        />
      </Grid>
    </Grid>
    
  );

}

export default Index;